package ru.frosteye.vknewtest.execution.exchange.common;

import com.google.gson.GsonBuilder;

import javax.inject.Inject;

import ru.frosteye.ovsa.di.qualifer.ApiUrl;
import ru.frosteye.ovsa.execution.network.client.BaseRetrofitClient;
import ru.frosteye.ovsa.execution.network.client.IdentityProvider;
import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;
import ru.frosteye.vknewtest.data.entity.container.PhotoInfo;
import ru.frosteye.vknewtest.execution.exchange.response.gson.AlbumInfoDeserializer;
import ru.frosteye.vknewtest.execution.exchange.response.gson.PhotoInfoDeserializer;

/**
 * Created by oleg on 23.10.17.
 */

public class RetrofitApiClient extends BaseRetrofitClient<Api> implements ApiClient {

    @Inject
    public RetrofitApiClient(@ApiUrl String baseUrl, IdentityProvider identityProvider) {
        super(baseUrl, identityProvider);
    }

    @Override
    public Class<Api> apiClass() {
        return Api.class;
    }

    @Override
    protected GsonBuilder createGsonBuilder() {
        GsonBuilder builder = super.createGsonBuilder();
        builder.registerTypeAdapter(AlbumInfo.class, new AlbumInfoDeserializer());
        builder.registerTypeAdapter(PhotoInfo.class, new PhotoInfoDeserializer());
        return builder;
    }

    @Override
    public void initWithUrl(String url) {
        init(url, getIdentityProvider());
    }
}
