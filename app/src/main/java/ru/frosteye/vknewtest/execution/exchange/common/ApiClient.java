package ru.frosteye.vknewtest.execution.exchange.common;

/**
 * Created by oleg on 23.10.17.
 */

public interface ApiClient {
    Api getApi();
    void initWithUrl(String url);
}
