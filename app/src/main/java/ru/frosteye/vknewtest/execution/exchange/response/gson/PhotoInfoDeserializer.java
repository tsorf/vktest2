package ru.frosteye.vknewtest.execution.exchange.response.gson;

import ru.frosteye.ovsa.execution.serialization.AdapterItemDeserializer;
import ru.frosteye.vknewtest.data.entity.Album;
import ru.frosteye.vknewtest.data.entity.Photo;
import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;
import ru.frosteye.vknewtest.data.entity.container.PhotoInfo;

/**
 * Created by oleg on 23.10.17.
 */

public class PhotoInfoDeserializer extends AdapterItemDeserializer<Photo, PhotoInfo> {
    @Override
    protected Class<Photo> provideType() {
        return Photo.class;
    }

    @Override
    protected Class<PhotoInfo> provideWrapperType() {
        return PhotoInfo.class;
    }
}
