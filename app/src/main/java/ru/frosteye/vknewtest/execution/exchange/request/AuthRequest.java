package ru.frosteye.vknewtest.execution.exchange.request;

import ru.frosteye.ovsa.execution.network.request.RequestParams;
import ru.frosteye.vknewtest.execution.exchange.common.Keys;
import ru.frosteye.vknewtest.execution.exchange.common.VkAuth;

/**
 * Created by oleg on 23.10.17.
 */

public class AuthRequest extends RequestParams {

    private long state = System.currentTimeMillis();

    public AuthRequest() {
        addParam(Keys.CLIENT_ID, VkAuth.APP_ID);
        addParam(Keys.DISPLAY, Keys.PAGE);
        addParam(Keys.SCOPE, VkAuth.SCOPE);
        addParam(Keys.REDIRECT_URI, VkAuth.REDIRECT);
        addParam(Keys.RESPONSE_TYPE, Keys.TOKEN);
        addParam(Keys.V, VkAuth.API_VERSION);
        addParam(Keys.STATE, state);
    }

    public long getState() {
        return state;
    }
}
