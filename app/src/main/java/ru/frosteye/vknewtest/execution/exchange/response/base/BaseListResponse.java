package ru.frosteye.vknewtest.execution.exchange.response.base;

import java.util.List;

/**
 * Created by oleg on 23.10.17.
 */

public class BaseListResponse<T> {

    private ItemsResponse<T> response;

    public ItemsResponse<T> getResponse() {
        return response;
    }
}
