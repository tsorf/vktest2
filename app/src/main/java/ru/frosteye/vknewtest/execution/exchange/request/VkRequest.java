package ru.frosteye.vknewtest.execution.exchange.request;

import ru.frosteye.ovsa.execution.network.request.RequestParams;
import ru.frosteye.vknewtest.execution.exchange.common.Keys;
import ru.frosteye.vknewtest.execution.exchange.common.VkAuth;

/**
 * Created by oleg on 23.10.17.
 */

public class VkRequest extends RequestParams {

    public VkRequest() {
        addParam(Keys.V, VkAuth.API_VERSION);
    }
}
