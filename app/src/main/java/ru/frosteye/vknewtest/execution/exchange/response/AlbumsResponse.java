package ru.frosteye.vknewtest.execution.exchange.response;

import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;
import ru.frosteye.vknewtest.execution.exchange.response.base.BaseListResponse;

/**
 * Created by oleg on 23.10.17.
 */

public class AlbumsResponse extends BaseListResponse<AlbumInfo> {
}
