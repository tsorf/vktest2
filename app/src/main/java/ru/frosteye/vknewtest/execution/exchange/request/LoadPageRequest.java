package ru.frosteye.vknewtest.execution.exchange.request;

/**
 * Created by oleg on 23.10.17.
 */

public class LoadPageRequest {
    private int entityId;
    private int page;

    public int getEntityId() {
        return entityId;
    }

    public LoadPageRequest setEntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }

    public int getPage() {
        return page;
    }

    public LoadPageRequest setPage(int page) {
        this.page = page;
        return this;
    }
}
