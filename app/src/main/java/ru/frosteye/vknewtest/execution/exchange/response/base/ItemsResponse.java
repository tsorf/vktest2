package ru.frosteye.vknewtest.execution.exchange.response.base;

import java.util.List;

/**
 * Created by oleg on 23.10.17.
 */

public class ItemsResponse<T> {

    private List<T> items;

    public List<T> getItems() {
        return items;
    }
}
