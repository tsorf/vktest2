package ru.frosteye.vknewtest.execution.exchange.common;

/**
 * Created by oleg on 23.10.17.
 */

public class VkAuth {
    public static final String API_VERSION = "5.68";
    public static final String APP_ID = "6230883";
    public static final String SCOPE = "photos";
    public static final String REDIRECT = "http://frosteye.ru/callback";
}
