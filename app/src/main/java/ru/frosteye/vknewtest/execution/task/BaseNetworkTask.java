package ru.frosteye.vknewtest.execution.task;

import java.io.IOException;
import java.util.concurrent.Executor;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import retrofit2.Call;
import retrofit2.Response;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.ovsa.execution.network.base.ApiException;
import ru.frosteye.ovsa.execution.task.ObservableTask;
import ru.frosteye.vknewtest.R;
import ru.frosteye.vknewtest.data.repo.contract.UserRepo;
import ru.frosteye.vknewtest.execution.exchange.common.Api;
import ru.frosteye.vknewtest.execution.exchange.common.Keys;
import ru.frosteye.vknewtest.execution.exchange.request.VkRequest;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.getString;

/**
 * Created by oleg on 23.10.17.
 */

public abstract class BaseNetworkTask<Res> extends ObservableTask<VkRequest, Res> {

    private Api api;
    private UserRepo userRepo;

    public BaseNetworkTask(MainThread mainThread,
                           Executor executor, Api api, UserRepo userRepo) {
        super(mainThread, executor);
        this.api = api;
        this.userRepo = userRepo;
    }

    public UserRepo getUserRepo() {
        return userRepo;
    }

    @Override
    protected Observable<Res> prepareObservable(VkRequest params) {
        return Observable.create(e -> {
            params.addParam(Keys.ACCESS_TOKEN, userRepo.load().getAccessToken());
            onExecute(params, e);
        });
    }

    protected abstract void onExecute(VkRequest request, ObservableEmitter<Res> subscriber);

    protected <T> T executeCall(Call<T> call) {
        try {
            Response<T> response = call.execute();
            if(!response.isSuccessful()) {
                throw new ApiException(getString(R.string.connection_error), response.code());
            } else return response.body();
        } catch (IOException e) {
            throw new ApiException(getString(R.string.connection_error), 0);
        }
    }

    public Api getApi() {
        return api;
    }
}
