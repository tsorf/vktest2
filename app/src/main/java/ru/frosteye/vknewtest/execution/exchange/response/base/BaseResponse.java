package ru.frosteye.vknewtest.execution.exchange.response.base;

/**
 * Created by oleg on 23.10.17.
 */

public class BaseResponse<T> {

    private T response;

    public T getResponse() {
        return response;
    }
}
