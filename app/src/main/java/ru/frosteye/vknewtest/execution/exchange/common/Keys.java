package ru.frosteye.vknewtest.execution.exchange.common;

/**
 * Created by oleg on 23.10.17.
 */

public class Keys {
    public static final String CLIENT_ID = "client_id";
    public static final String DISPLAY = "display";
    public static final String PAGE = "page";
    public static final String REDIRECT_URI = "redirect_uri";
    public static final String RESPONSE_TYPE = "response_type";
    public static final String TOKEN = "token";
    public static final String V = "v";
    public static final String SCOPE = "scope";
    public static final String STATE = "state";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String USER_ID = "user_id";
    public static final String OFFSET = "offset";
    public static final String COUNT = "count";
    public static final String NEED_COVERS = "need_covers";
    public static final String THUMB_SRC = "thumb_src";
    public static final String ALBUM = "album";
    public static final String PHOTO_75 = "photo_75";
    public static final String PHOTO_130 = "photo_130";
    public static final String PHOTO_604 = "photo_604";
    public static final String PHOTO_807 = "photo_807";
    public static final String PHOTO_1280 = "photo_1280";
    public static final String PHOTO_2560 = "photo_2560";
    public static final String ALBUM_ID = "album_id";
}
