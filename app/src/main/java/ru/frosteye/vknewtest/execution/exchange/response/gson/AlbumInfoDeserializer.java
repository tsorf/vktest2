package ru.frosteye.vknewtest.execution.exchange.response.gson;

import com.google.gson.JsonDeserializer;

import ru.frosteye.ovsa.execution.serialization.AdapterItemDeserializer;
import ru.frosteye.vknewtest.data.entity.Album;
import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;

/**
 * Created by oleg on 23.10.17.
 */

public class AlbumInfoDeserializer extends AdapterItemDeserializer<Album,  AlbumInfo> {
    @Override
    protected Class<Album> provideType() {
        return Album.class;
    }

    @Override
    protected Class<AlbumInfo> provideWrapperType() {
        return AlbumInfo.class;
    }
}
