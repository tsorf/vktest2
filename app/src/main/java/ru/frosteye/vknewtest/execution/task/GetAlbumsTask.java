package ru.frosteye.vknewtest.execution.task;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import io.reactivex.ObservableEmitter;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;
import ru.frosteye.vknewtest.data.repo.contract.UserRepo;
import ru.frosteye.vknewtest.execution.exchange.common.Api;
import ru.frosteye.vknewtest.execution.exchange.request.VkRequest;
import ru.frosteye.vknewtest.execution.exchange.response.AlbumsResponse;

/**
 * Created by oleg on 23.10.17.
 */

public class GetAlbumsTask extends BaseNetworkTask<List<AlbumInfo>> {

    @Inject
    public GetAlbumsTask(MainThread mainThread,
                         Executor executor,
                         Api api,
                         UserRepo userRepo) {
        super(mainThread, executor, api, userRepo);
    }

    @Override
    protected void onExecute(VkRequest request,
                             ObservableEmitter<List<AlbumInfo>> subscriber) {
        try {
            AlbumsResponse response = executeCall(getApi().getAlbums(request));
            subscriber.onNext(response.getResponse().getItems());
            subscriber.onComplete();
        } catch (Exception e) {
            subscriber.onError(e);
        }
    }
}
