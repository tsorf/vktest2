package ru.frosteye.vknewtest.execution.exchange.common;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import ru.frosteye.vknewtest.execution.exchange.request.VkRequest;
import ru.frosteye.vknewtest.execution.exchange.response.AlbumsResponse;
import ru.frosteye.vknewtest.execution.exchange.response.PhotosResponse;

/**
 * Created by oleg on 23.10.17.
 */

public interface Api {

    @POST("photos.getAlbums")
    Call<AlbumsResponse> getAlbums(@QueryMap VkRequest request);

    @POST("photos.get")
    Call<PhotosResponse> getPhotos(@QueryMap VkRequest request);
}
