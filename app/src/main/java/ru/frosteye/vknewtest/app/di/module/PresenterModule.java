package ru.frosteye.vknewtest.app.di.module;

import android.view.View;

import ru.frosteye.vknewtest.app.di.qualifier.AlbumsPresenter;
import ru.frosteye.vknewtest.app.di.qualifier.PhotosPresenter;
import ru.frosteye.vknewtest.app.di.scope.PresenterScope;
import ru.frosteye.vknewtest.data.entity.Album;
import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;
import ru.frosteye.vknewtest.data.entity.container.PhotoInfo;
import ru.frosteye.vknewtest.presentation.presenter.contract.ItemListPresenter;
import ru.frosteye.vknewtest.presentation.presenter.contract.LoginPresenter;
import ru.frosteye.vknewtest.presentation.presenter.impl.AlbumsPresenterImpl;
import ru.frosteye.vknewtest.presentation.presenter.impl.LoginPresenterImpl;
import ru.frosteye.vknewtest.presentation.presenter.impl.PhotosPresenterImpl;
import ru.frosteye.vknewtest.presentation.view.contract.ItemListView;
import ru.frosteye.vknewtest.presentation.view.impl.activity.BaseActivity;
import ru.frosteye.vknewtest.presentation.view.impl.fragment.BaseFragment;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.ovsa.di.module.BasePresenterModule;

@Module
public class PresenterModule extends BasePresenterModule<BaseActivity, BaseFragment> {
    public PresenterModule(View view) {
        super(view);
    }

    public PresenterModule(BaseActivity activity) {
        super(activity);
    }

    public PresenterModule(BaseFragment fragment) {
        super(fragment);
    }

    @Provides @PresenterScope
    LoginPresenter provideLoginPresenter(LoginPresenterImpl presenter) {
        return presenter;
    }
    
    @Provides @AlbumsPresenter
    ItemListPresenter<AlbumInfo, ItemListView<AlbumInfo>> provideAlbumsPresenter(AlbumsPresenterImpl presenter) {
        return presenter;
    }
    
    @Provides @PhotosPresenter
    ItemListPresenter<PhotoInfo, ItemListView<PhotoInfo>> providePhotosPresenter(PhotosPresenterImpl presenter) {
        return presenter;
    }
}
