package ru.frosteye.vknewtest.app.di.component;

import ru.frosteye.vknewtest.app.di.module.AppModule;
import ru.frosteye.vknewtest.app.di.module.PresenterModule;
import ru.frosteye.vknewtest.app.di.module.RepoModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class, RepoModule.class})
@Singleton
public interface AppComponent {
    PresenterComponent plus(PresenterModule module);
}