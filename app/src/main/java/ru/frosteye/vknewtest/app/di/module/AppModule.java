package ru.frosteye.vknewtest.app.di.module;

import com.facebook.drawee.backends.pipeline.Fresco;

import ru.frosteye.ovsa.di.qualifer.ApiUrl;
import ru.frosteye.ovsa.execution.network.client.IdentityProvider;
import ru.frosteye.vknewtest.R;
import ru.frosteye.vknewtest.app.environment.VkTest;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.ovsa.di.module.BaseAppModule;
import ru.frosteye.vknewtest.data.repo.contract.UserRepo;
import ru.frosteye.vknewtest.execution.exchange.common.Api;
import ru.frosteye.vknewtest.execution.exchange.common.ApiClient;
import ru.frosteye.vknewtest.execution.exchange.common.RetrofitApiClient;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.*;


@Module
public class AppModule extends BaseAppModule<VkTest> {

    public AppModule(VkTest context) {
        super(context);
        Fresco.initialize(context);
    }

    @Provides @ApiUrl
    String provideApiUrl() {
        return context.getString(R.string.config_api_url);
    }

    @Provides @Singleton
    Api provideApi(ApiClient apiClient) {
        return apiClient.getApi();
    }

    @Provides @Singleton
    ApiClient provideApiClient(RetrofitApiClient apiClient) {
        return apiClient;
    }

    @Provides @Singleton
    IdentityProvider provideIdentity(UserRepo repo) {
        return repo;
    }
}
