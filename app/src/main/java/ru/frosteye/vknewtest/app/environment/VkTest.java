package ru.frosteye.vknewtest.app.environment;

import android.app.Application;

import ru.frosteye.vknewtest.app.di.component.AppComponent;
import ru.frosteye.vknewtest.app.di.component.DaggerAppComponent;
import ru.frosteye.vknewtest.app.di.module.AppModule;


public class VkTest extends Application {

    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
