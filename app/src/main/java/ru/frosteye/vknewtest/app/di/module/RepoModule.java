package ru.frosteye.vknewtest.app.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.vknewtest.data.repo.contract.UserRepo;
import ru.frosteye.vknewtest.data.repo.impl.UserRepoImpl;

@Module
public class RepoModule {

    @Provides @Singleton
    UserRepo provideUserRepo(UserRepoImpl repo) {
        return repo;
    }
}
