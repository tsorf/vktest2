package ru.frosteye.vknewtest.app.di.component;

import ru.frosteye.vknewtest.app.di.module.PresenterModule;
import ru.frosteye.vknewtest.app.di.scope.PresenterScope;
import ru.frosteye.vknewtest.presentation.view.impl.activity.AlbumsActivity;
import ru.frosteye.vknewtest.presentation.view.impl.activity.BaseActivity;
import ru.frosteye.vknewtest.presentation.view.impl.activity.ListActivity;
import ru.frosteye.vknewtest.presentation.view.impl.activity.LoginActivity;
import ru.frosteye.vknewtest.presentation.view.impl.activity.PhotosActivity;
import ru.frosteye.vknewtest.presentation.view.impl.fragment.BaseFragment;

import dagger.Subcomponent;

@PresenterScope
@Subcomponent(modules = PresenterModule.class)
public interface PresenterComponent {
    void inject(BaseFragment baseFragment);

    void inject(BaseActivity baseActivity);
    void inject(LoginActivity activity);
    void inject(AlbumsActivity activity);
    void inject(PhotosActivity activity);
}
