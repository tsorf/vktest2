package ru.frosteye.vknewtest.data.entity.container;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.vknewtest.R;
import ru.frosteye.vknewtest.data.entity.Album;
import ru.frosteye.vknewtest.presentation.view.impl.widget.AlbumView;

/**
 * Created by oleg on 23.10.17.
 */

public class AlbumInfo extends AdapterItem<Album, AlbumView> {
    @Override
    public int getLayoutRes() {
        return R.layout.view_album;
    }
}
