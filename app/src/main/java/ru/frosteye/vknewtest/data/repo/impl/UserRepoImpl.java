package ru.frosteye.vknewtest.data.repo.impl;

import javax.inject.Inject;

import ru.frosteye.ovsa.data.storage.BaseRepo;
import ru.frosteye.ovsa.data.storage.Storage;
import ru.frosteye.ovsa.execution.network.client.IdentityProvider;
import ru.frosteye.vknewtest.data.entity.User;
import ru.frosteye.vknewtest.data.repo.contract.UserRepo;

/**
 * Created by oleg on 23.10.17.
 */

public class UserRepoImpl extends BaseRepo<User> implements UserRepo, IdentityProvider {

    @Inject
    public UserRepoImpl(Storage storage) {
        super(storage);
    }

    @Override
    public String provideIdentity() {
        return null;
    }

    @Override
    protected Class<User> provideClass() {
        return User.class;
    }
}
