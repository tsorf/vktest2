package ru.frosteye.vknewtest.data.entity.container;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.vknewtest.R;
import ru.frosteye.vknewtest.data.entity.Album;
import ru.frosteye.vknewtest.data.entity.Photo;
import ru.frosteye.vknewtest.presentation.view.impl.widget.AlbumView;
import ru.frosteye.vknewtest.presentation.view.impl.widget.PhotoView;

/**
 * Created by oleg on 23.10.17.
 */

public class PhotoInfo extends AdapterItem<Photo, PhotoView> {
    @Override
    public int getLayoutRes() {
        return R.layout.view_photo;
    }


}
