package ru.frosteye.vknewtest.data.repo.contract;

import ru.frosteye.ovsa.data.storage.Repo;
import ru.frosteye.ovsa.execution.network.client.IdentityProvider;
import ru.frosteye.vknewtest.data.entity.User;

/**
 * Created by oleg on 23.10.17.
 */

public interface UserRepo extends Repo<User>, IdentityProvider {
}
