package ru.frosteye.vknewtest.data.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ru.frosteye.vknewtest.execution.exchange.common.Keys;

/**
 * Created by oleg on 23.10.17.
 */

public class Album implements Serializable {

    private int id;

    @SerializedName(Keys.THUMB_SRC)
    private String thumbSrc;

    private String title;
    private String description;

    public int getId() {
        return id;
    }

    public String getThumbSrc() {
        return thumbSrc;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
