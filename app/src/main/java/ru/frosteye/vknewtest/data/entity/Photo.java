package ru.frosteye.vknewtest.data.entity;

import com.google.gson.annotations.SerializedName;

import ru.frosteye.vknewtest.execution.exchange.common.Keys;

/**
 * Created by oleg on 23.10.17.
 */

public class Photo {

    private String text;

    @SerializedName(Keys.PHOTO_75)
    private String photo75;

    @SerializedName(Keys.PHOTO_130)
    private String photo130;

    @SerializedName(Keys.PHOTO_604)
    private String photo604;

    @SerializedName(Keys.PHOTO_807)
    private String photo807;

    @SerializedName(Keys.PHOTO_1280)
    private String photo1280;

    @SerializedName(Keys.PHOTO_2560)
    private String photo2560;

    public String getText() {
        return text;
    }

    public String getPhoto75() {
        return photo75;
    }

    public String getPhoto130() {
        return photo130;
    }

    public String getPhoto604() {
        return photo604;
    }

    public String getPhoto807() {
        return photo807;
    }

    public String getPhoto1280() {
        return photo1280;
    }

    public String getPhoto2560() {
        return photo2560;
    }

    public String findLargestPhoto() {
        if(photo2560 != null) return photo2560;
        if(photo1280 != null) return photo1280;
        if(photo807 != null) return photo807;
        if(photo604 != null) return photo604;
        if(photo130 != null) return photo130;
        if(photo75 != null) return photo75;
        return null;
    }

    public String findSmallestPhoto() {
        if(photo75 != null) return photo75;
        if(photo130 != null) return photo130;
        if(photo604 != null) return photo604;
        if(photo807 != null) return photo807;
        if(photo1280 != null) return photo1280;
        if(photo2560 != null) return photo2560;
        return null;
    }

    public String findMiddlePhoto() {
        if(photo604 != null) return photo604;
        if(photo807 != null) return photo807;
        if(photo130 != null) return photo130;
        if(photo1280 != null) return photo1280;
        if(photo75 != null) return photo75;
        if(photo2560 != null) return photo2560;
        return null;
    }
}
