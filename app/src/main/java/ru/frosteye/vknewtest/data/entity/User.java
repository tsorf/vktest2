package ru.frosteye.vknewtest.data.entity;

/**
 * Created by oleg on 23.10.17.
 */

public class User {
    private long userId;
    private String accessToken;

    public User(String accessToken, long userId) {
        this.userId = userId;
        this.accessToken = accessToken;
    }

    public long getUserId() {
        return userId;
    }

    public String getAccessToken() {
        return accessToken;
    }
}
