package ru.frosteye.vknewtest.presentation.view.impl.activity;

import javax.inject.Inject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.stfalcon.frescoimageviewer.ImageViewer;

import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.vknewtest.app.di.component.PresenterComponent;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.vknewtest.R;
import ru.frosteye.vknewtest.app.di.qualifier.AlbumsPresenter;
import ru.frosteye.vknewtest.app.di.qualifier.PhotosPresenter;
import ru.frosteye.vknewtest.data.entity.Album;
import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;
import ru.frosteye.vknewtest.data.entity.container.PhotoInfo;
import ru.frosteye.vknewtest.execution.exchange.common.Keys;
import ru.frosteye.vknewtest.execution.exchange.request.LoadPageRequest;
import ru.frosteye.vknewtest.presentation.presenter.contract.ItemListPresenter;
import ru.frosteye.vknewtest.presentation.view.contract.ItemListView;

public class PhotosActivity extends ListActivity<PhotoInfo> implements ItemListView<PhotoInfo> {

    @Inject @PhotosPresenter ItemListPresenter<PhotoInfo, ItemListView<PhotoInfo>> presenter;


    @Override
    protected ItemListPresenter<PhotoInfo, ItemListView<PhotoInfo>> getListPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    protected void prepareLoadPageRequest(LoadPageRequest request) {
        Album album = ((Album) getIntent().getSerializableExtra(Keys.ALBUM));
        setTitle(album.getTitle());
        request.setEntityId(album.getId());
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        enableBackButton();
    }

    @Override
    protected void prepareRecyclerView() {
    }

    @Override
    protected LinearLayoutManager createLayoutManager() {
        return new GridLayoutManager(this, 3);
    }

    @Override
    protected void onItemClick(PhotoInfo item) {
        View view = LayoutInflater.from(this).inflate(R.layout.view_title_overlay, null);
        TextView title = ((TextView) view.findViewById(R.id.activity_photos_title));
        new ImageViewer.Builder<>(this, getItems())
                .setFormatter(o -> o.getModel().findLargestPhoto())
                .setStartPosition(getItems().indexOf(item))
                .hideStatusBar(false)
                .allowZooming(true)
                .allowSwipeToDismiss(true)
                .setImageChangeListener(position -> title.setText(getItems().get(position).getModel().getText()))
                .setBackgroundColorRes(R.color.colorBlack)
                .setOverlayView(view)
                .show();
    }
}
