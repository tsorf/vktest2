package ru.frosteye.vknewtest.presentation.presenter.impl;

import javax.inject.Inject;

import ru.frosteye.vknewtest.data.entity.User;
import ru.frosteye.vknewtest.data.repo.contract.UserRepo;
import ru.frosteye.vknewtest.presentation.view.contract.LoginView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.vknewtest.presentation.presenter.contract.LoginPresenter;

public class LoginPresenterImpl extends BasePresenter<LoginView> implements LoginPresenter {

    private UserRepo userRepo;

    @Inject
    public LoginPresenterImpl(UserRepo userRepo) {

        this.userRepo = userRepo;
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onUserInfoReady(String accessToken, String userId) {
        userRepo.save(new User(accessToken, Long.parseLong(userId)));
        view.proceed();
    }
}
