package ru.frosteye.vknewtest.presentation.view.impl.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseLinearLayout;
import ru.frosteye.ovsa.presentation.view.widget.BaseRelativeLayout;
import ru.frosteye.vknewtest.R;
import ru.frosteye.vknewtest.data.entity.Album;

/**
 * Created by oleg on 23.10.17.
 */

public class AlbumView extends BaseLinearLayout implements ModelView<Album> {

    @BindView(R.id.view_album_description) TextView description;
    @BindView(R.id.view_album_title) TextView title;
    @BindView(R.id.view_album_image) ImageView image;

    private Album model;

    public AlbumView(Context context) {
        super(context);
    }

    public AlbumView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AlbumView(Context context, AttributeSet attrs,
                     int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AlbumView(Context context, AttributeSet attrs, int defStyleAttr,
                     int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        ButterKnife.bind(this);
    }

    @Override
    public Album getModel() {
        return model;
    }

    @Override
    public void setModel(Album model) {
        this.model = model;
        this.title.setText(model.getTitle());
        this.description.setText(model.getDescription());
        if(model.getThumbSrc() != null) {
            Picasso.with(getContext())
                    .load(model.getThumbSrc())
                    .fit().centerCrop()
                    .into(image);
        } else {
            image.setImageDrawable(null);
        }
    }
}
