package ru.frosteye.vknewtest.presentation.presenter.impl;

import java.util.List;

import javax.inject.Inject;

import ru.frosteye.ovsa.execution.task.SimpleSubscriber;
import ru.frosteye.vknewtest.R;
import ru.frosteye.vknewtest.data.entity.Album;
import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;
import ru.frosteye.vknewtest.data.entity.container.PhotoInfo;
import ru.frosteye.vknewtest.execution.exchange.common.Keys;
import ru.frosteye.vknewtest.execution.exchange.request.LoadPageRequest;
import ru.frosteye.vknewtest.execution.exchange.request.VkRequest;
import ru.frosteye.vknewtest.execution.task.GetPhotosTask;
import ru.frosteye.vknewtest.presentation.presenter.contract.ItemListPresenter;
import ru.frosteye.vknewtest.presentation.view.contract.ItemListView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.getInteger;

public class PhotosPresenterImpl extends BasePresenter<ItemListView<PhotoInfo>>
        implements ItemListPresenter<PhotoInfo, ItemListView<PhotoInfo>> {

    private int photosPerPage;
    private GetPhotosTask getPhotosTask;
    private Album album;

    @Inject
    public PhotosPresenterImpl(GetPhotosTask getPhotosTask) {
        this.photosPerPage = getInteger(R.integer.config_photos_page_count);
        this.getPhotosTask = getPhotosTask;
    }

    @Override
    public void onStop() {
        getPhotosTask.cancel();
    }

    @Override
    public void onAttach(ItemListView<PhotoInfo> photoInfoItemListView) {
        super.onAttach(photoInfoItemListView);
    }

    @Override
    public void onLoadPage(LoadPageRequest loadPageRequest) {
        enableControls(false);
        VkRequest request = new VkRequest();
        request.addParam(Keys.OFFSET, loadPageRequest.getPage() * photosPerPage);
        request.addParam(Keys.COUNT, photosPerPage);
        request.addParam(Keys.ALBUM_ID, loadPageRequest.getEntityId());
        getPhotosTask.execute(request, new SimpleSubscriber<List<PhotoInfo>>() {
            @Override
            public void onError(Throwable e) {
                enableControls(true);
                showMessage(e.getMessage());
            }

            @Override
            public void onNext(List<PhotoInfo> albumInfos) {
                enableControls(true);
                view.appendItems(albumInfos);
            }
        });
    }
}
