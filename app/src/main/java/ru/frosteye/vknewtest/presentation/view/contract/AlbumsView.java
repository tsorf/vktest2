package ru.frosteye.vknewtest.presentation.view.contract;

import java.util.List;

import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;

public interface AlbumsView extends ItemListView<AlbumInfo> {
}
