package ru.frosteye.vknewtest.presentation.presenter.contract;

import ru.frosteye.vknewtest.data.entity.User;
import ru.frosteye.vknewtest.presentation.view.contract.LoginView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface LoginPresenter extends LivePresenter<LoginView> {
    void onUserInfoReady(String accessToken, String userId);
}
