package ru.frosteye.vknewtest.presentation.view.impl.activity;

import android.content.Intent;

import javax.inject.Inject;

import ru.frosteye.vknewtest.app.di.component.PresenterComponent;
import ru.frosteye.vknewtest.app.di.qualifier.AlbumsPresenter;
import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;
import ru.frosteye.vknewtest.execution.exchange.common.Keys;
import ru.frosteye.vknewtest.presentation.presenter.contract.ItemListPresenter;
import ru.frosteye.vknewtest.presentation.view.contract.AlbumsView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.vknewtest.presentation.view.contract.ItemListView;
import ru.frosteye.vknewtest.presentation.view.impl.widget.AlbumView;

public class AlbumsActivity extends ListActivity<AlbumInfo> implements ItemListView<AlbumInfo> {

    @Inject @AlbumsPresenter ItemListPresenter<AlbumInfo, ItemListView<AlbumInfo>> presenter;


    @Override
    protected ItemListPresenter<AlbumInfo, ItemListView<AlbumInfo>> getListPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    protected void onItemClick(AlbumInfo item) {
        Intent intent = new Intent(this, PhotosActivity.class);
        intent.putExtra(Keys.ALBUM, item.getModel());
        startActivity(intent);
    }
}
