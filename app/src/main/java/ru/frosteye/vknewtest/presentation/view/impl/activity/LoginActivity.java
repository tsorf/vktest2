package ru.frosteye.vknewtest.presentation.view.impl.activity;

import javax.inject.Inject;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import ru.frosteye.vknewtest.app.di.component.PresenterComponent;
import ru.frosteye.vknewtest.execution.exchange.request.AuthRequest;
import ru.frosteye.vknewtest.execution.exchange.common.Keys;
import ru.frosteye.vknewtest.presentation.presenter.contract.LoginPresenter;
import ru.frosteye.vknewtest.presentation.view.contract.LoginView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.vknewtest.R;

public class LoginActivity extends BaseActivity implements LoginView {

    @BindView(R.id.activity_login_enter) Button enter;
    @BindView(R.id.activity_login_webView) WebView webView;

    @Inject LoginPresenter presenter;

    private AuthRequest request;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        showTopBarLoading(!enabled);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        enter.setOnClickListener(v -> login());
        webView.setWebViewClient(new WebClient());
        webView.getSettings().setJavaScriptEnabled(true);
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public void login() {
        enter.setVisibility(View.GONE);
        request = new AuthRequest();
        webView.loadUrl(getString(R.string.config_api_auth_url, request.toQueryString()));
    }

    @Override
    public void proceed() {
        startActivityAndClearTask(AlbumsActivity.class);
    }

    private class WebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            enableControls(false, 0);
            if(url.startsWith("http://frosteye.ru/callback")) {
                try {
                    processResponse(url);
                } catch (Exception e) {
                    e.printStackTrace();
                    showMessage(e.getMessage());
                }

            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            enableControls(true, 0);
        }
    }

    private void processResponse(String url) throws Exception {
        String query = url.split("#")[1];
        String[] parts = query.split("&");
        Map<String, String> map = new HashMap<>();
        for(String string: parts) {
            String[] param = string.split("=");
            map.put(param[0], param[1]);
        }
        presenter.onUserInfoReady(map.get(Keys.ACCESS_TOKEN), map.get(Keys.USER_ID));
    }

    @Override
    public void onBackPressed() {
        if(webView.canGoBack()) {
            webView.goBack();
            return;
        }
        super.onBackPressed();
    }
}
