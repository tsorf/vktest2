package ru.frosteye.vknewtest.presentation.view.impl.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseRelativeLayout;
import ru.frosteye.vknewtest.R;
import ru.frosteye.vknewtest.data.entity.Album;
import ru.frosteye.vknewtest.data.entity.Photo;

/**
 * Created by oleg on 23.10.17.
 */

public class PhotoView extends BaseRelativeLayout implements ModelView<Photo> {

    @BindView(R.id.view_photo_image) ImageView image;

    private Photo model;

    public PhotoView(Context context) {
        super(context);
    }

    public PhotoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PhotoView(Context context, AttributeSet attrs,
                     int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PhotoView(Context context, AttributeSet attrs, int defStyleAttr,
                     int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        ButterKnife.bind(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    public Photo getModel() {
        return model;
    }

    @Override
    public void setModel(Photo model) {
        this.model = model;
        String smallest = model.findMiddlePhoto();
        if(smallest != null) {
            Picasso.with(getContext())
                    .load(smallest)
                    .fit().centerCrop()
                    .into(image);
        } else {
            image.setImageDrawable(null);
        }
    }
}
