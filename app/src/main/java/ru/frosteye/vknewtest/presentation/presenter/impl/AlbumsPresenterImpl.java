package ru.frosteye.vknewtest.presentation.presenter.impl;

import java.util.List;

import javax.inject.Inject;

import ru.frosteye.ovsa.execution.task.SimpleSubscriber;
import ru.frosteye.vknewtest.R;
import ru.frosteye.vknewtest.data.entity.container.AlbumInfo;
import ru.frosteye.vknewtest.execution.exchange.common.Keys;
import ru.frosteye.vknewtest.execution.exchange.request.LoadPageRequest;
import ru.frosteye.vknewtest.execution.exchange.request.VkRequest;
import ru.frosteye.vknewtest.execution.task.GetAlbumsTask;
import ru.frosteye.vknewtest.presentation.presenter.contract.ItemListPresenter;
import ru.frosteye.vknewtest.presentation.view.contract.AlbumsView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.vknewtest.presentation.view.contract.ItemListView;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.getInteger;

public class AlbumsPresenterImpl extends BasePresenter<ItemListView<AlbumInfo>>
        implements ItemListPresenter<AlbumInfo, ItemListView<AlbumInfo>> {

    private GetAlbumsTask getAlbumsTask;
    private int albumsPerPage;

    @Inject
    public AlbumsPresenterImpl(GetAlbumsTask getAlbumsTask) {
        this.albumsPerPage = getInteger(R.integer.config_albums_page_count);
        this.getAlbumsTask = getAlbumsTask;
    }

    @Override
    public void onStop() {
        getAlbumsTask.cancel();
    }

    @Override
    public void onLoadPage(LoadPageRequest loadPageRequest) {
        enableControls(false);
        VkRequest request = new VkRequest();
        request.addParam(Keys.OFFSET, loadPageRequest.getPage() * albumsPerPage);
        request.addParam(Keys.COUNT, albumsPerPage);
        request.addParam(Keys.NEED_COVERS, 1);
        getAlbumsTask.execute(request, new SimpleSubscriber<List<AlbumInfo>>() {
            @Override
            public void onError(Throwable e) {
                enableControls(true);
                showMessage(e.getMessage());
            }

            @Override
            public void onNext(List<AlbumInfo> albumInfos) {
                enableControls(true);
                view.appendItems(albumInfos);
            }
        });
    }
}
