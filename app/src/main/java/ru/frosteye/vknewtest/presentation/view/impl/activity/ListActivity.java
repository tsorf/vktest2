package ru.frosteye.vknewtest.presentation.view.impl.activity;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFlexible;
import ru.frosteye.ovsa.presentation.adapter.FlexibleModelAdapter;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.ovsa.presentation.view.widget.ListDivider;
import ru.frosteye.ovsa.stub.impl.EndlessRecyclerOnScrollListener;
import ru.frosteye.vknewtest.R;
import ru.frosteye.vknewtest.execution.exchange.request.LoadPageRequest;
import ru.frosteye.vknewtest.presentation.presenter.contract.ItemListPresenter;
import ru.frosteye.vknewtest.presentation.view.contract.ItemListView;

/**
 * Created by oleg on 23.10.17.
 */

public abstract class ListActivity<T extends IFlexible> extends BaseActivity implements FlexibleAdapter.OnItemClickListener, ItemListView<T> {

    @BindView(R.id.activity_list_list) RecyclerView list;
    @BindView(R.id.activity_list_swipe) SwipeRefreshLayout swipe;

    private FlexibleModelAdapter<T> adapter;
    private EndlessRecyclerOnScrollListener onScrollListener;
    private LoadPageRequest loadPageRequest = new LoadPageRequest();
    private final List<T> items = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        showTopBarLoading(!enabled);
        if(enabled) swipe.setRefreshing(false);
    }

    protected void prepareLoadPageRequest(LoadPageRequest request) {

    }

    @Override @CallSuper
    protected void initView(Bundle savedInstanceState) {
        prepareLoadPageRequest(loadPageRequest);
        LinearLayoutManager linearLayoutManager = createLayoutManager();
        onScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                getListPresenter().onLoadPage(loadPageRequest.setPage(currentPage));
            }
        };
        swipe.setOnRefreshListener(() -> {
            onScrollListener.reset();
            adapter.clear();
            getListPresenter().onLoadPage(loadPageRequest.setPage(0));
        });
        list.setLayoutManager(linearLayoutManager);
        list.addOnScrollListener(onScrollListener);
        prepareRecyclerView();
        adapter = new FlexibleModelAdapter<>(items, this);
        list.setAdapter(adapter);
    }

    public List<T> getItems() {
        return items;
    }

    protected LinearLayoutManager createLayoutManager() {
        return new LinearLayoutManager(this);
    }

    protected void prepareRecyclerView() {
        list.addItemDecoration(new ListDivider(this, ListDivider.VERTICAL_LIST));
    }

    @Override
    protected void attachPresenter() {
        getListPresenter().onAttach(this);
        getListPresenter().onLoadPage(loadPageRequest.setPage(0));
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return getListPresenter();
    }

    protected abstract ItemListPresenter<T, ItemListView<T>> getListPresenter();



    @Override
    public void appendItems(List<T> albums) {
        adapter.addItems(adapter.getItemCount(), albums);
    }

    @Override
    public boolean onItemClick(int position) {
        T item = adapter.getItem(position);
        onItemClick(item);
        return true;
    }

    protected abstract void onItemClick(T item);
}
