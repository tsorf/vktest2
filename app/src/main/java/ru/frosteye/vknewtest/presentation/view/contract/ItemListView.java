package ru.frosteye.vknewtest.presentation.view.contract;

import java.util.List;

import ru.frosteye.ovsa.presentation.view.BasicView;

/**
 * Created by oleg on 23.10.17.
 */

public interface ItemListView<T> extends BasicView {
    void appendItems(List<T> items);
}
