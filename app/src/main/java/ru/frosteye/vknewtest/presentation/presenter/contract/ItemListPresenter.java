package ru.frosteye.vknewtest.presentation.presenter.contract;

import ru.frosteye.vknewtest.execution.exchange.request.LoadPageRequest;
import ru.frosteye.vknewtest.presentation.view.contract.AlbumsView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.vknewtest.presentation.view.contract.ItemListView;

public interface ItemListPresenter<T, V extends ItemListView<T>> extends LivePresenter<V> {
    void onLoadPage(LoadPageRequest request);
}
