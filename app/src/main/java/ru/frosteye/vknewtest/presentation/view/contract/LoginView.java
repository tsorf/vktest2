package ru.frosteye.vknewtest.presentation.view.contract;

import ru.frosteye.ovsa.presentation.view.BasicView;

public interface LoginView extends BasicView {
    void login();
    void proceed();
}
